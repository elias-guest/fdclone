fdclone (3.01j-1) unstable; urgency=medium

  * New upstream release
  * Update Standard-Version to 4.5.0
  * Update debian/control VCS-* path
  * Add debian/control Rules-Requires:no
  * Update years in debian/copyright
  * Remove tab-in-license-text in debian/copyright

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Thu, 30 Apr 2020 19:43:11 -0500

fdclone (3.01h-1) unstable; urgency=medium

  * New upstream release
  * Updated Standard-Version to 4.3.0.1
  * Bump debhelper to 12
  * Updated debian/control VCS-* fields
  * Updated copyright years

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sat, 02 Feb 2019 11:24:18 -0500

fdclone (3.01e-1) unstable; urgency=medium

  * New upstream release
  * Updated Standard-Version to 4.1.3
  * Bump debhelper to 11
  * Updated debian/control VCS-* fields
  * debian/rules adding hardening rules

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sun, 25 Feb 2018 13:57:24 -0300

fdclone (3.01b-1) unstable; urgency=low

  * New upstream release
  * Updated Standard-Version to 3.9.5
  * Updated debian/control canonicalize VCS-* fields
  * debian/patches/20_lintian_manpage_warning.patch: Removed Upstream fix
  * debian/patches/40_lintian_manpage_spelling.patch: Removed Upstream fix

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sun, 27 Jul 2014 23:57:13 -0500

fdclone (3.01-1) unstable; urgency=low

  * New upstream release
  * Updated Standard-Version to 3.9.4
  * Bump debhelper to 9
  * debian/rules enabled hardening build flags

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sun, 31 Mar 2013 20:19:19 -0500

fdclone (3.00k-1) unstable; urgency=low

  * New upstream release
  * Updated Standard-Version to 3.9.3
  * Bump debhelper to 8
  * Remove Build-Depends on quilt
  * debian/copyright update format as Debian Policy 3.9.3
  * debian/rules update cleaning rules
  * Move packaging to collab-maint
  * Corrected some lintian warnings:
    - Updated debian-copyright to UTF-8 instead obsolete national encoding.
    - Fixed duplicate changelog file.

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sun, 13 May 2012 17:38:11 -0500

fdclone (3.00j-2) unstable; urgency=low

  * Fixed some troubles with translation table.
  * Fixed purging package, properly remove the configuration file.
  * Fixed some spelling errors in the manual page.

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sat, 27 Nov 2010 20:59:31 -0500

fdclone (3.00j-1) unstable; urgency=low

  * New maintainer. (Closes: #494602)
  * Updated Standards-Version to 3.9.1
  * Rewrote Debian build system with debhelper and quilt.
  * debian/patches/20_lintian_manpage_warning.patch: Added.
  * debian/patches/30_makefilein_setting.patch: Added.
  * Fixed debian/watch error.
  * Corrected some lintian warnings:
    +Fixed issues with manpage.
    +Updated compat version.

 -- Elías Alejandro Año Mendoza <ealmdz@gmail.com>  Sat, 16 Oct 2010 09:50:01 -0500

fdclone (3.00c-1) unstable; urgency=low

  * QA upload.
  * Acknowledge NMU: thanks to Luk
  * New upstream release; Closes: #475345
  * debian/control
    - added dpatch build-dep
    - added Homepage field
    - set maintainer to QA Group
    - bump Standars-Version to 3.8.0
    - removed article from short description
  * debian/rules
    - added dpatch stuff
    - removed comment header
    - removed explicit gzip of examples, dh_compress will take care of that
    - removed commented dh_* calls
    - added variables to make install for manpage
    - don't ignore clean error
    - removed custom doc-base file installation, dh_installdocs will take care
    - fixed MANDIR location
    - removed custom fdsh.1 installation, dh_installmans will take care
    - added mkmf.sed removal to clean target
    - removed custom installation of example, dh_installexamples will take care
    - added removal of install-stamp file
  * debian/patches/01_previous_changes.dpatch
    - created to avoid direct upstream code changes
    - updated to new upstream code
  * debian/menu
    - updated section to Applications/File Management
  * debian/copyright
    - converted to UTF-8
  * debian/{fdclone-faq,fdclone-readme}
    - updated section to File Management
    - removed bottom empty line
    - renamed to fdclone.doc-base.<ext> to let dh_installdocs do its job
  * debian/postinst
    - removed custom doc-base installation, left only touching of conf file
  * debian/prerm
    - removed since contained only doc-base removal, handle by dh_installdeb
  * debian/README.source
    - added due to Policy 3.8.0
  * debian/copyright
    - indented upstream author with 4 spaces
    - added copyright section
    - clearly identified license section
    - added the new verbatim licences (English and Japanese)
  * debian/addition/fdsh.1
    - fixed hyphen-used-as-minus-sign
  * debian/patches/10_bts436823_remove_strip_makefile.dpatch
    - added to remove unconditional binary stripping; thanks to Julien Danjou
      for the report; Closes: #436823

 -- Sandro Tosi <matrixhasu@gmail.com>  Wed, 27 Aug 2008 23:08:14 +0200

fdclone (2.06c-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS due to kernel syscall macros (Closes: #393027).
  * Add $(MAKE) config to debian/rules (Closes: #394091).

 -- Luk Claes <luk@debian.org>  Mon, 23 Oct 2006 21:00:28 +0200

fdclone (2.06c-1) unstable; urgency=low

  * New upstream release

 -- Taketoshi Sano <sano@debian.org>  Sat, 19 Mar 2005 20:48:35 +0900

fdclone (2.04a-1) unstable; urgency=low

  * New upstream release
  * update debian/watch. now I can use "uscan" command. :)

 -- Taketoshi Sano <sano@debian.org>  Sat,  9 Aug 2003 09:58:31 +0900

fdclone (2.04-1) unstable; urgency=low

  * New upstream release

 -- Taketoshi Sano <sano@debian.org>  Thu, 26 Jun 2003 23:04:42 +0900

fdclone (2.02a-1) unstable; urgency=low

  * New upstream release (Closes: #174592)

 -- Taketoshi Sano <sano@debian.org>  Tue, 31 Dec 2002 19:56:36 +0900

fdclone (2.01-2) unstable; urgency=low

  * Apply patch from the upstream author to fix a buffer under flow
    bug which causes freeze at incorrect shell script.
  * fix Debian specific bug which causes trouble in Japanese filename
    handling on MS-DOS drive.

 -- Taketoshi Sano <sano@debian.org>  Tue, 21 May 2002 01:51:58 +0900

fdclone (2.01-1) unstable; urgency=low

  * New upstream release (Closes: #146446)
  * Update system default configuration file /etc/fdclone/fd2rc
    according to the advice from the upstream author in order to
    handle the difference of key codes between some terminals.

 -- Taketoshi Sano <sano@debian.org>  Tue, 14 May 2002 07:51:20 +0900

fdclone (2.00b-1) unstable; urgency=low

  * New upstream release.  Closes: #140588
  * Remove the dependencies on PAGERS/EDITORS to conform
    the Debian Policy 12.4 "Editors and pagers". Also
    remove the settings for PAGERS/EDITORS from the default
    site configuration file (/etc/fdclone/fd2rc).
    Closes: #140600
  * Add required condition to machine.h to build on ia64
    Closes: #142051

 -- Taketoshi Sano <sano@debian.org>  Mon, 29 Apr 2002 08:31:27 +0900

fdclone (2.00a-1.1) unstable; urgency=low

  * local build test

 -- Taketoshi Sano <sano@debian.org>  Wed, 20 Feb 2002 21:16:21 +0900

fdclone (2.00a-1) unstable; urgency=low

  * New upstream release. (Closes: #129344)
  * moved into main from non-free as the result of
    the license change from 2.00
  * package subsection updated to shells from utils
  * update debian specific changes for new v2 scheme.

 -- Taketoshi Sano <sano@debian.org>  Tue, 19 Feb 2002 23:56:56 +0900

fdclone (1.03t-4) unstable; urgency=low

  * OK, lynx doesn't use "less". It was my misunderstandings.
    so lynx is now added in Depends line.
    (But you should take care when you use lynx as PAGER, because
     binary file make it crazy.  OTOH, w3m can show the compressed
     files correctly using /usr/bin/sensible-pager with gzip -d -c.)

 -- Taketoshi Sano <sano@debian.org>  Mon,  5 Nov 2001 21:42:24 +0900

fdclone (1.03t-3) unstable; urgency=low

  * update the examples to match the change in the location of
    configuration file.

 -- Taketoshi Sano <sano@debian.org>  Wed, 17 Oct 2001 23:11:02 +0900

fdclone (1.03t-2) unstable; urgency=low

  * added vim to Depends line
    (lynx as pager just uses "less" so I don't think
     lynx is good as w3m for PAGER here, though.
     vim is nice to have in Depends as EDITOR, agreed.)
  * remove ae from Depends line
    (Closes: #115821)

 -- Taketoshi Sano <sano@debian.org>  Wed, 17 Oct 2001 02:17:39 +0900

fdclone (1.03t-1) unstable; urgency=low

  * New upstream release.
  * Additional terminal setting files are removed:
       /etc/fdrc.linux, /etc/fdrc.kterm, and /etc/fdrc.xterm
  * Default configuration file is /etc/fdclone/fdrc now,
    and users' site specific configuration can be put into
    /etc/fdclone/fdrc.siteconfig.
  * No more source code modification for PAGER and EDITOR.
    These settings are provided from /etc/fdclone/fdrc only.

 -- Taketoshi Sano <sano@debian.org>  Sat, 29 Sep 2001 18:03:45 +0900

fdclone (1.03s-1) unstable; urgency=low

  * New upstream release. (not uploaded, local only version)

 -- Taketoshi Sano <sano@debian.org>  Wed, 25 Jul 2001 00:31:20 +0900

fdclone (1.03r-2) unstable; urgency=low

  * update copyright year (2000 -> 2001)
  * comment out the keymap setting in /etc/fdrc, and newly add
    /etc/fdrc.linux, /etc/fdrc.kterm, /etc/fdrc.xterm as conffiles
    because the previous setup does not work well on kon and krxvt
    (Thanks to Norita-san).
  * English manpage is added (this is partial translation and
    not so awfully useful one, but it may be better than nothing).

 -- Taketoshi Sano <sano@debian.org>  Wed,  2 May 2001 17:28:17 +0900

fdclone (1.03r-1) unstable; urgency=low

  * New upstream version
  * applied unofficial patch
  * revert to use the customized _fdrc file, and describe about this
    modification in README.Debian

 -- Taketoshi Sano <sano@debian.org>  Wed, 25 Apr 2001 00:11:14 +0900

fdclone (1.03q-1) unstable; urgency=low

  * New upstream version
  * change to use the default _fdrc file instead of
    customized one.  Old customized file is saved in
    examples.

 -- Taketoshi Sano <sano@debian.org>  Fri,  2 Mar 2001 14:44:49 +0900

fdclone (1.03p-2) unstable; urgency=low

  * try to fix build fail on alpha (closes #85813).
    Thanks to Paul Slootman for his report & patch.

 -- Taketoshi Sano <sano@debian.org>  Wed, 14 Feb 2001 23:18:31 +0900

fdclone (1.03p-1) unstable; urgency=low

  * New upstream version.

 -- Taketoshi Sano <sano@debian.org>  Tue, 28 Nov 2000 22:08:47 +0900

fdclone (1.03o-2) unstable; urgency=low

  * update debian/watch
  * update the author's e-mail address in copyright file
  * fix debian/_fdrc.{k,x}term for some keymaps

 -- Taketoshi Sano <sano@debian.org>  Tue, 24 Oct 2000 22:45:57 +0900

fdclone (1.03o-1) unstable; urgency=low

  * New upstream version.

 -- Taketoshi Sano <sano@debian.org>  Thu, 12 Oct 2000 11:42:20 +0900

fdclone (1.03n-1) unstable; urgency=low

  * New Maintainer
  * New upstream version.

 -- Taketoshi Sano <sano@debian.org>  Wed,  4 Oct 2000 00:00:23 +0900

fdclone (1.03l-7) unstable; urgency=low

  * Signed with GNUPG.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Sat,  1 Apr 2000 14:21:45 +0900

fdclone (1.03l-6) unstable; urgency=low

  * When building binary, DEFRC should be /etc.  FIXED.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Sun, 20 Feb 2000 22:29:54 +0900

fdclone (1.03l-5) unstable; urgency=low

  * Again, I was stupid enough to compile it with DEBIAN_BUILDARCH=pentiumpro.
    FIXED.  I gave up to use pentium-builder.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Thu,  6 Jan 2000 14:38:50 +0900

fdclone (1.03l-4) unstable; urgency=low

  * Now _fdrc(original config file) is installed instead of fdrc.
  * Added sample .fdrc.linux and .fdrc.kterm.
    (Thanks again hamasaki@main.eng.hokudai.ac.jp)

 -- Masayuki Hatta <mhatta@debian.or.jp>  Mon,  3 Jan 2000 17:17:40 +0900

fdclone (1.03l-3) unstable; urgency=low

  * Deleted Debian templates.
  * fdrc: added sample keymap entries for tty and kterm.
    (Thanx hamasaki@main.eng.hokudai.ac.jp)

 -- Masayuki Hatta <mhatta@debian.or.jp>  Thu, 30 Dec 1999 00:19:53 +0900

fdclone (1.03l-2) unstable; urgency=low

  * I was stupid enough to compile it with DEBIAN_BUILDARCH=pentiumpro.
    Fixed.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Thu, 18 Nov 1999 00:01:22 +0900

fdclone (1.03l-1) unstable; urgency=low

  * New Upstream Version.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Mon,  8 Nov 1999 16:06:16 +0900

fdclone (1.03k-0.4) unstable; urgency=low

  * Restored dosdisc.c.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Thu, 23 Sep 1999 14:20:14 +0900

fdclone (1.03k-0.3) unstable; urgency=low

  * Applied a patch from the upstream author.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Wed, 22 Sep 1999 16:40:46 +0900

fdclone (1.03k-0.2) unstable; urgency=low

  * Fixed several typos in debian/README.Debian.
  * Fixed several goofs in debian/rules.
  * Compliance with FHS 2.0.

 -- Masayuki Hatta <mhatta@debian.or.jp>  Wed,  8 Sep 1999 10:51:56 +0900

fdclone (1.03k-0.1) unstable; urgency=low

  * New Maintainer(temporarily, though).
  * New upstrem version.
  * Followed most of the modification the former maintainer did.
  * Tweaked /etc/fdrc(.deb support added).

 -- Masayuki Hatta <mhatta@debian.or.jp>  Wed,  1 Sep 1999 15:44:20 +0900

fdclone (1.03h-1) unstable; urgency=low

  * Upstream upgrade.
  * Move "#include <sys/ioctl.h>" from machine.h to term.c.
  * Install "fd-unicd.tbl" to /usr/lib instead of /usr/bin.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.ne.jp>  Sun, 25 Oct 1998 18:25:19 +0900

fdclone (1.03g-3) frozen unstable; urgency=low

  * Changed distribution.
  * Changed section to non-free/utils.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.ne.jp>  Mon, 10 Aug 1998 19:35:51 +0900

fdclone (1.03g-2) non-free-jp; urgency=low

  * Moved japanese man directory from ja_JP.ujis to ja.
  * Changed maintainer's e-mail address from or.jp to ne.jp.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.ne.jp>  Sat,  8 Aug 1998 17:55:28 +0900

fdclone (1.03g-1) non-free-jp; urgency=low

  * Upstream upgrade.
  * Compiled with libc6.
  * Added "#include <sys/ioctl.h>" to machine.h.
  * Change the default editor and pager in /etc/fdrc to
    /usr/bin/sensible-editor and /usr/bin/sensible-pager. [Bug#JP/282]
  * Fix segmentation fault ocurring when archiver definition is not set.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.or.jp>  Sun, 17 May 1998 01:15:45 +0900
fdclone (1.03a-1) non-free-jp; urgency=low

  * Upstream upgrade.
  * Copy _fdrc to fdrc (which will be installed to /etc/fdrc).
  * Move the distribution to "non-free" because of restriction on
    distributing with binary form.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.or.jp>  Wed, 24 Sep 1997 00:24:57 +0900

fdclone (1.01h-2) unstable; urgency=low

  * ja_JP.EUC -> ja_JP.ujis

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.or.jp>  Sun, 27 Jul 1997 12:10:05 +0900

fdclone (1.01h-1) unstable; urgency=low

  * Copy .fdrc to fdrc.
  * Initial Release.

 -- TAKAHASHI Katsuyuki <hashi@ppp.fastnet.or.jp>  Sun, 29 Jun 1997 12:02:23 +0900
